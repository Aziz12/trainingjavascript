//  Buatlah variable untuk menyimpan data username dan jumlah item yang ada pada keranjang
var inputUsername = 'aziz';
var inputJumlahItem = "3";

var username = inputUsername;
var jumlahItemKeranjang = parseInt(inputJumlahItem); 

console.log("Username:", username);
console.log("Jumlah Item Keranjang:", jumlahItemKeranjang);

// Buatlah sebuah perhitungan untuk menghitung total harga dari setiap item yang ditambahkan ke keranjang
var keranjang = [];

// Buatlah sebuah fungsi untuk menyimpan item pada keranjang
function tambahkanItem(nama, harga, jumlah) {
    var item = {
        nama: nama,
        harga: harga,
        jumlah: jumlah
    };
    keranjang.push(item);
}
 
function hitungTotalHarga() {
    var totalHarga = 0;
 
    for (var i = 0; i < keranjang.length; i++) {
        var item = keranjang[i];
        totalHarga += item.harga * item.jumlah;
    }

    return totalHarga;
}
 
tambahkanItem("Buku", 5000, 2);
tambahkanItem("Pensil", 3000, 3);
tambahkanItem("Monitor", 2000000, 1);
 
var totalHargaKeranjang = hitungTotalHarga();
 
console.log("Isi Keranjang:", keranjang);
console.log("Total Harga Keranjang:", totalHargaKeranjang);


// Buatlah sebuah arrow function untuk menghitung total harga setelah diskon
const hitungTotalHargaDiskon = (hargaPerItem, jumlahItem, diskon) => {
    const hargaSebelumDiskon = hargaPerItem * jumlahItem;
    const diskonAmount = (diskon / 100) * hargaSebelumDiskon;
    const hargaSetelahDiskon = hargaSebelumDiskon - diskonAmount;

    return hargaSetelahDiskon;
};
 
const totalHargaSetelahDiskon = hitungTotalHargaDiskon(50, 3, 10);
 
console.log("Total Harga Setelah Diskon:", totalHargaSetelahDiskon);


// Buatlah contoh penggunaan sebuah variable global untuk menghitung total dengan ongkos kirim
var totalHarga = 0;
 
function tambahkanItemKeranjang(nama, harga, jumlah) {
    var itemTotal = harga * jumlah;
    totalHarga += itemTotal;
    return itemTotal;
}
 
function hitungTotalDenganOngkosKirim(ongkosKirim) {
    var totalDenganOngkos = totalHarga + ongkosKirim;
    return totalDenganOngkos;
}
 
var totalItem1 = tambahkanItemKeranjang("Susu Murni", 5000, 2);
var totalItem2 = tambahkanItemKeranjang("Tahu Bulat 1Kg", 25000, 3);
 
var totalDenganOngkosKirim = hitungTotalDenganOngkosKirim(10000);
 
console.log("Total Harga Item 1:", totalItem1);
console.log("Total Harga Item 2:", totalItem2);
console.log("Total Harga Keseluruhan:", totalHarga);
console.log("Total dengan Ongkos Kirim:", totalDenganOngkosKirim);


// buatlah class Product
class Product {
    constructor(  name, price, id) { 
        this.name = name;
        this.price = price;
        this.id = id;
    } 
}
 
const pizzaProduct = new Product("Sepatu", "902.000", "1");

console.log('Product Details:', pizzaProduct);



//  Buatlah sebuah Asynchronous function tunuk mendapatkan data product dari API
async function getDataProduct() {
    try {
        const response = await fetch('products.json');
        const data = await response.json();

        console.log('Data Produk:', data);
    } catch (error) {
        console.error('Gagal mendapatkan data produk:', error.message);
    }
} 
getDataProduct(); 


// buatlah sebuah fungsi untuk mengambil data product detail dari API
async function getProductDetail(productId) {
    try { 
        const response = await fetch('details.json');
 
        if (!response.ok) {
            throw new Error('HTTP error! Status: ${response.status}');
        }
 
        const data = await response.json();

        // Menampilkan detail produk sesuai dengan ID yang diminta
        const productDetail = data.find(product => product.id === productId);

        if (productDetail) {
            console.log('Detail Produk:', productDetail);
        } else {
            console.error(`Produk dengan ID ${productId} tidak ditemukan.`);
        }
    } catch (error) { 
        console.error('Gagal mendapatkan detail produk:', error.message);
    }
}
 
getProductDetail(1);
